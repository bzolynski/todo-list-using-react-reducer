import React, { useRef, useReducer } from "react";
import { v4 as uuidv4 } from "uuid";

const actions = {
	ADD: "ADD",
	MOVE_TO_DONE: "MOVE_TO_DONE",
	MOVE_TO_TODO: "MOVE_TO_TODO",
};

const taskStatuses = {
	TODO: "TODO",
	DONE: "DONE",
};

function reducer(state, action) {
	switch (action.type) {
		case actions.ADD:
			return {
				...state,
				list: [
					...state.list,
					{ id: action.id, title: action.title, status: taskStatuses.TODO }
				]
			};
		case actions.MOVE_TO_DONE:
			return {
				...state,
				list: state.list.map(task =>
					task.id === action.id ? { ...task, status: taskStatuses.DONE } : task
				)
			};
		case actions.MOVE_TO_TODO:
			return {
				...state,
				list: state.list.map(task =>
					task.id === action.id ? { ...task, status: taskStatuses.TODO } : task
				)
			};
		default:
			return state;
	}
}

export default function App() {
	const [state, dispatch] = useReducer(reducer, { list: [] });
	const taskTitleRef = useRef();

	const addTask = () => {
		dispatch({ id: uuidv4(), type: actions.ADD, title: taskTitleRef.current.value });
		taskTitleRef.current.value = "";
	};

	return (
		<div className="App">
			<input type="text" ref={taskTitleRef} />
			<button type="button" onClick={addTask}>
				Add task
			</button>
			<ul style={{ listStyleType: "none" }}>
				{state.list.map((task, index) => (
					<li key={task.id}>
						<span>{index + 1}.&nbsp;</span>
						<span>{task.title}&nbsp;</span>
						<span>Status: {task.status}&nbsp;</span>
						{task.status === taskStatuses.TODO ? (
							<button
								type="button"
								onClick={() => dispatch({ type: actions.MOVE_TO_DONE, id: task.id })}
							>
								SET AS DONE
							</button>
						) : (
							<button
								type="button"
								onClick={() => dispatch({ type: actions.MOVE_TO_TODO, id: task.id })}
							>
								SET AS TODO
							</button>
						)}
					</li>
				))}
			</ul>
		</div>
	);
}
